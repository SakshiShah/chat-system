/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatsystem.constants;

/**
 *
 * @author rohan
 */
public interface ChatsystemConstants {
    public String FILEPATH = "./xmlfiles/";
    public String SEND = "<Send>*</Send>";
    public String MESSAGE = "<Message>*</Message>";
    public String SEEN = "<Seen>*</Seen>";
    public String RECEIVE = "<Receive>*</Receive>";
}
