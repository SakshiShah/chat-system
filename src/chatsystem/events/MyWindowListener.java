/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatsystem.events;

import chatsystem.db.MySQLConnect;
import chatsystem.opeartions.ClientCode;
import chatsystem.ui.Dashboard;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 *
 * @author rohan
 */
public class MyWindowListener implements WindowListener{
    private Dashboard dashboard;
    private ClientCode clientCode;
    
    public MyWindowListener(Dashboard dashboard, ClientCode clientCode){
        this.dashboard = dashboard;
        this.clientCode = clientCode;
    }

    @Override
    public void windowClosing(WindowEvent e) {
        Socket socket = clientCode.getSocket();
        try{
            PrintWriter output = new PrintWriter(socket.getOutputStream(), true);
            output.println("$" + dashboard.getMyName());
            System.out.println("Windowclosing!");
        }catch(IOException ex){
            System.out.println("IOException while windowClosing : " + ex.getMessage());
        }
    }
    
    @Override
    public void windowOpened(WindowEvent e) {}

    @Override
    public void windowClosed(WindowEvent e) {}

    @Override
    public void windowIconified(WindowEvent e) {}

    @Override
    public void windowDeiconified(WindowEvent e) {}

    @Override
    public void windowActivated(WindowEvent e) {}

    @Override
    public void windowDeactivated(WindowEvent e) {}
    
}
