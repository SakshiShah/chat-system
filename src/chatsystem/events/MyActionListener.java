/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatsystem.events;

import chatsystem.opeartions.ClientCode;
import chatsystem.ui.Dashboard;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author rohan
 */
public class MyActionListener implements ActionListener{
    private Dashboard dashboard;
    //private ClientCode clientCode;
    public MyActionListener(Dashboard dashboard){
        this.dashboard = dashboard;
    }
    
    public void actionPerformed(ActionEvent ae){
        if(ae.getActionCommand().equals("Send")){
            
            ClientCode.startClientSend(dashboard.getMyName(), dashboard.gettaMessage().getText(), dashboard.getUsernameLabel().getText());
            dashboard.gettaMessage().setText("");
        }
    }
}
