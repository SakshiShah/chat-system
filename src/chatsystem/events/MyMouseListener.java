/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatsystem.events;

import chatsystem.opeartions.ClientCode;
import chatsystem.opeartions.ClientReceive;
import chatsystem.ui.Dashboard;
import chatsystem.ui.UsernamePanel;
import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JPanel;

/**
 *
 * @author rohan
 */
public class MyMouseListener implements MouseListener {

    private Dashboard dashboard;
    private ClientCode clientCode;

    public MyMouseListener(Dashboard dashboard) {
        this.dashboard = dashboard;
    }

    @Override
    public void mouseClicked(MouseEvent me) {
        UsernamePanel panel = (UsernamePanel) me.getComponent();
        dashboard.getUsernameLabel().setText(panel.getName());
        dashboard.getChatScreen().removeAll();
    }
    
    @Override
    public void mousePressed(MouseEvent me) {
    }
    
    @Override
    public void mouseEntered(MouseEvent me) {
    }
    
    @Override
    public void mouseExited(MouseEvent me) {
    }

    @Override
    public void mouseReleased(MouseEvent me) {
    }

}
