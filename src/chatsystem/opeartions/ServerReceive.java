/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatsystem.opeartions;

import chatsystem.db.MySQLConnect;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author rohan
 */
public class ServerReceive implements Runnable{
    private Thread t;
    private Map<String, Socket> hashMap;
    private Socket socket;
    private ArrayList<String> registeredUsers;
    public ServerReceive(Socket socket, Map<String, Socket> hashMap, ArrayList<String> registeredUsers){
        this.socket = socket;
        this.hashMap = hashMap;
        this.registeredUsers = registeredUsers;
        
        t = new Thread(this);
        t.start();
    }
    
    private void sendRegisteredArrayList(Socket socket, String clientName, boolean addInList){
        String users = "`";
        for(int i = 0; i<registeredUsers.size(); i++){
            users += registeredUsers.get(i) + ",,";
        }
        try{
            PrintWriter output = new PrintWriter(socket.getOutputStream(), true);
            output.println(users);
        }catch(IOException e){
            System.out.println("IOException caught : " + e.getMessage());
        }
        if(addInList){
            registeredUsers.add(clientName);
            sendMessageToAll("`"+clientName+",,");
        }
    }
    
    private void sendOnlineUsers(Socket socket, String clientName){
        String users = "@";
        if(hashMap.size() > 1){
            for(String user : hashMap.keySet()){
                if(!user.equals(clientName)){
                    users += user + ",,";
                }
            }
             try{
                PrintWriter output = new PrintWriter(socket.getOutputStream(), true);
                output.println(users);
            }catch(IOException e){
                System.out.println("IOException caught : " + e.getMessage());
            }
        }
    }
    
    private void sendMessageToAll(String message){
        for(String clientName : hashMap.keySet()){
            Socket socket = hashMap.get(clientName);
            new ServerSend(socket, message);
        }
    }
    
    private void broadcastUserStatus(String clientName, boolean isOnline){
        for(String client : hashMap.keySet()){
            if(!client.equals(clientName)){
                Socket socket = hashMap.get(client);
                if(isOnline){
                    //User came online
                    new ServerSend(socket, "@"+clientName+",,");
                }else{
                    //User gone offline
                    new ServerSend(socket, "$"+clientName+",,");
                }
            }
        }
    }
    
    @Override
    public void run(){
        try{
            BufferedReader input;
            String message, toClientName;
            Socket toSendSocket;
            while(true){
                input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                message = input.readLine();
                
                //Server receiving clientName for first time to put the name as key in hashMap
                String temp = message.charAt(0)+"";
                String clientName = message.substring(1);
                if(temp.equals("?")){
                    hashMap.put(clientName, socket);
                    if(!registeredUsers.contains(clientName)){
                        sendRegisteredArrayList(socket, clientName, true);
                    }else{
                        //clientName is already registered.
                        sendRegisteredArrayList(socket, clientName, false);
                    }
                    
                    broadcastUserStatus(clientName, true);
                    sendOnlineUsers(socket, clientName);
                    
                    continue;
                }else if("$".equals(temp)){
                    //User gone offline
                    broadcastUserStatus(clientName, false);
                    continue;
                }
                
                //Normal Message passing
                System.out.println("Message : " +  message);
                toClientName = message.substring(message.indexOf(":") + 1, message.indexOf("::"));
                System.out.println("ToClientName : " + toClientName);
                if(toClientName.equals("broadcast")){
                    //BroadCasting message
                    sendMessageToAll(message);
                }else if(hashMap.containsKey(toClientName)){
                    //Message sending to the specified 'clientName'
                    System.out.println("Server Message sending : " + message);
                    toSendSocket = hashMap.get(toClientName);
                    new ServerSend(toSendSocket, "~"+message);
                }
                
                
            }
        }catch(IOException e){
            System.out.println("ServerReceive IOException : " + e.getMessage());
        }
    }
}
