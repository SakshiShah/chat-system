/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatsystem.opeartions;

import chatsystem.ui.Dashboard;
import java.awt.Color;
import java.awt.FlowLayout;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author rohan
 */
public class ClientSend implements Runnable{
    private Thread t;
    private Socket socket;
    private Dashboard dashboard;
    private String message;
    private String myName;
    private String toClientName;
    public ClientSend(Socket socket, Dashboard dashboard, String myName, String message, String toClientName){
        this.socket = socket;
        this.dashboard = dashboard;
        this.myName = myName;
        this.message = message;
        this.toClientName = toClientName;
        
        t = new Thread(this);
        t.start();
    }
    
    public void run(){
        try{
            PrintWriter output;
            String messageFormat;
            output = new PrintWriter(socket.getOutputStream(), true);
            messageFormat = myName + ":" + toClientName + "::" + message;
            output.println(messageFormat);
            JPanel chatScreen = dashboard.getChatScreen();
            JPanel messagePanel = new JPanel();
            messagePanel.setLayout(new FlowLayout(FlowLayout.RIGHT, 10, 5));
            messagePanel.setBackground(Color.WHITE);
            messagePanel.setSize(698, 388);
            JLabel text = new JLabel(message);
            text.setForeground(Color.BLUE);
            messagePanel.add(text);
            chatScreen.add(messagePanel);
            chatScreen.validate();
            dashboard.validate();
        }catch(IOException e){
            System.out.println("ClientSend IOException : " + e.getMessage());
        }
    }
}
