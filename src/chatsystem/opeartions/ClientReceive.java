/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatsystem.opeartions;

import chatsystem.constants.ChatsystemConstants;
import chatsystem.ui.Dashboard;
import chatsystem.ui.UsernamePanel;
import java.awt.Color;
import java.awt.FlowLayout;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Scanner;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author rohan
 */
public class ClientReceive implements Runnable{
    private Thread t;
    private Socket socket;
    private Dashboard dashboard;
    private String myName;
    JLabel label = null;
    String message = "", fromClient = "";
    public ClientReceive(Socket socket, Dashboard dashboard, String myName){
        this.socket = socket;
        this.dashboard = dashboard;
        this.myName = myName;
        
        t = new Thread(this);
        t.start();
    }
    public void run(){
       try{
           
           //Creating the file for 'myName' when the reading thread starts
           dashboard.createFile();
           
           while(true){
                System.out.println("-------------");
                BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                message = input.readLine();
                System.out.println("Input readLine : " + message);
                if("`".equals(message.charAt(0) + "")){
                    //A new User was registered!
                    dashboard.addRegisteredUsersList(message.substring(1, message.length()-2));
                    continue;
                }else if("@".equals(message.charAt(0) + "")){
                    //A User came online
                    setOnlineOfflineUser(message.substring(1), true);
                    continue;
                }else if("$".equals(message.charAt(0)+"")){
                    //A User gone offline
                    setOnlineOfflineUser(message.substring(1), false);
                }else if(message.contains("~")){
                    //Normal message
                    //Message Format : "~fromClient:myName::message"
                    message = message.substring(1);
                    fromClient = message.substring(0, message.indexOf(":"));
                    System.out.println("FromClient : " + fromClient);
                    message = message.substring(message.indexOf("::"));
                    message = message.substring(2);
                    setMessagePanel(message);
                    
                    prepareFileContents(fromClient, message);
                }
           }
       }catch(IOException e){
                System.out.println("Client Receive : " + e);
        }  
    }
    
    private void setOnlineOfflineUser(String onlineUser, boolean isOnline){
        Scanner scanner = new Scanner(onlineUser);
        scanner.useDelimiter(",,");
        while(scanner.hasNext()){
            UsernamePanel panel = (UsernamePanel)dashboard.getFriendListPanel().getComponent(dashboard.getRegisteredUsersList().indexOf(scanner.next()));
            JLabel label = panel.getLabel();
            if(isOnline){
                label.setForeground(Color.BLUE);
            }else{
                label.setForeground(Color.BLACK);
            }
            dashboard.validate();
        }
    }
    
    private void setMessagePanel(String message){
        JPanel chatScreen = dashboard.getChatScreen();
        JPanel messagePanel = new JPanel();
        messagePanel.setSize(698, 388);
        messagePanel.setLayout(new FlowLayout(FlowLayout.LEFT, 10, 5));
        messagePanel.add(new JLabel(message));
        chatScreen.add(messagePanel);
        dashboard.validate();
        
    }
    
    private void prepareFileContents(String fromClient, String message){
        String contents = ChatsystemConstants.RECEIVE + "\r\n";
        String temp = ChatsystemConstants.MESSAGE + "\r\n";
        temp = temp.replace("*", message);
        contents = contents.replace("*", "\r\n\t" + temp);
        System.out.println(contents);
        dashboard.writeToFile(fromClient, contents);
    }

}
