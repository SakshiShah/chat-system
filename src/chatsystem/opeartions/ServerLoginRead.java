/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatsystem.opeartions;

import chatsystem.db.MySQLConnect;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

/**
 *
 * @author rohan
 */
public class ServerLoginRead implements Runnable{
    private Thread t;
    private Socket socket;
    private Connection conn;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    
    public ServerLoginRead(Socket socket){
        this.socket = socket;
        conn = MySQLConnect.getConnection();
        
        t = new Thread(this);
        t.start();
    }
    
    @Override
    public void run(){
        try{
            Scanner scanner = new Scanner(new InputStreamReader(socket.getInputStream()));
            scanner.useDelimiter(":");
            String option = scanner.next();
            scanner.skip(scanner.delimiter());
            String username = scanner.next();
            scanner.skip(scanner.delimiter());
            String password = scanner.nextLine();
            
            if(option.equalsIgnoreCase("login")){
                new ServerLoginWrite(socket, check(username, password));
            }else if(option.equalsIgnoreCase("register")){
                new ServerLoginWrite(socket, insert(username, password));
            }
            
            
        }catch(IOException e){
            System.out.println("ServerLoginRead IOException e : " + e.getMessage());
        }
    }
    
    private boolean check(String username, String password){
        try{
            String sql = "SELECT * FROM user WHERE username = ? AND password = ?";
            ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            ps.setString(2, password);
            
            rs = ps.executeQuery();
            if(rs.next()){
                sql = "UPDATE user SET status = 1 WHERE username = ?";
                ps = conn.prepareStatement(sql);
                ps.setString(1, username);
                ps.execute();
                return true;
            }
            return false;
        }catch(SQLException e){
            System.out.println("ServerLoginRead SQLException while checking: " + e.getMessage());
        }
        return false;
    }
    
    private boolean insert(String username, String password){
        try{
            String sql = "INSERT INTO user(username,password, status) VALUES (?,?,1)";
            
            ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            ps.setString(2, password);
            ps.execute();
            return true;
        }catch(SQLException e){
            System.out.println("ServerLoginRead SQLException while inserting : " + e.getMessage());
        }
        return false;
    }
}
