/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatsystem.opeartions;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

/**
 *
 * @author rohan
 */
public class ServerLoginWrite implements Runnable{
    private Thread t;
    private Socket socket;
    private boolean status;
    public ServerLoginWrite(Socket socket, boolean status){
        
        this.socket = socket;
        this.status = status;
        
        t = new Thread(this);
        t.start();
    }
    
    @Override
    public void run(){
        try(PrintWriter output = new PrintWriter(socket.getOutputStream(), true)){
            output.println(status+"");
        }catch(IOException e){
            System.out.println("ServerLoginWrite IOException : " + e.getMessage());
        }
    }
}
