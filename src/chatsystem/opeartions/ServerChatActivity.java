/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatsystem.opeartions;

import chatsystem.db.MySQLConnect;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author rohan
 */
public class ServerChatActivity implements Runnable{
    private Thread t;
    private static Map<String, Socket> hashMap;
    private ArrayList<String> registeredUsers;
    public ServerChatActivity(){
        hashMap = new HashMap<>();
        registeredUsers = new ArrayList<>();
        readRegisteredUsers();
        
        t = new Thread(this);
        t.start();
    }
    
    private void readRegisteredUsers(){
        try{
            Connection conn = MySQLConnect.getConnection();
            Statement statement = conn.createStatement();
            ResultSet rs = statement.executeQuery("SELECT * FROM user");
            while(rs.next()){
                registeredUsers.add(rs.getString("username"));
            }
        }catch(SQLException e){
            System.out.println("SQLException while reading registered users from db : " + e);
        }
    }
    
    @Override
    public void run(){
        try(ServerSocket serverSocket = new ServerSocket(5000)){
            System.out.println("Server Started!");
            while(true){
                Socket socket = serverSocket.accept();
                new ServerReceive(socket, hashMap, registeredUsers);
            }
        }catch(IOException e){
            System.out.println("ServerCode IOException : " + e.getMessage());
        }
    }
}
