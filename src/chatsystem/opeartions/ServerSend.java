/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatsystem.opeartions;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

/**
 *
 * @author rohan
 */
public class ServerSend implements Runnable{
    private Thread t;
    private Socket socket;
    private String message;
    public ServerSend(Socket socket, String message){
        this.socket = socket;
        this.message = message;
        
        t = new Thread(this);
        t.start();
    }
    public void run(){
        try{
            PrintWriter output = new PrintWriter(socket.getOutputStream(), true);
            output.println(message);
        }catch(IOException e){
            System.out.println("ServerSend IOException : " + e.getMessage());
        }
    }
}
