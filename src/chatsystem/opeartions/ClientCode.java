/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatsystem.opeartions;

import chatsystem.ui.Dashboard;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author rohan
 */
public class ClientCode implements Runnable{
    private Thread t;
    private String myName;
    private static Socket socket;
    private static Dashboard dashboard;
    private ClientReceive clientReceive;
    public ClientCode(String myName, Dashboard dashboard){
        this.myName = myName;
        this.dashboard = dashboard;
        
        t = new Thread(this);
        t.start();
    }
    
    public void run(){
        try{
            socket = new Socket("localhost", 5000);

            //Sending the first message to the server which is 'myName'
            //Message : ?sakshishah
            PrintWriter output = new PrintWriter(socket.getOutputStream(), true);
            output.println("?"+myName);
            
            //The first message received from server will always be a list of registered users.
            BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String registeredUsers = input.readLine();
            if("`".equals(registeredUsers.charAt(0)+"")){
                registeredUsers = registeredUsers.substring(1);
                Scanner scanner = new Scanner(registeredUsers);
                scanner.useDelimiter(",,");

                while(scanner.hasNext()){
                    dashboard.addRegisteredUsersList(scanner.next());    
                }
            }
            
            clientReceive = new ClientReceive(socket, dashboard, myName);

            while(true){}
        }catch(IOException e){
            System.out.println("Client Code : " + e);
        }
    }
    
    public Socket getSocket(){
        return this.socket;
    }
    
    public ClientReceive getClientRecieve(){
        return this.clientReceive;
    }
    
    public static void startClientSend(String myName, String message, String toClientName){
        new ClientSend(ClientCode.socket, dashboard, myName, message, toClientName);
    }
    
}
