/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatsystem.opeartions;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author rohan
 */
public class ServerLogin implements Runnable{
    private Thread t;
    ServerLogin(){
        
        t = new Thread(this);
        t.start();
    }
    
    @Override
    public void run(){
        try(ServerSocket loginSocket = new ServerSocket(4000)){
            while(true){
                Socket socket = loginSocket.accept();
                new ServerLoginRead(socket);
                
            }
        }catch(IOException e){
            System.out.println("ServerLogin IOException : " + e.getMessage());
        }
    }
}
